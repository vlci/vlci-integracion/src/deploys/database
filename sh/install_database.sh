#!/bin/bash
set -o pipefail
set -e
RESULT=0
trap catch_errors ERR

function catch_errors() {
    RESULT=$?
}

# Global variables
CONFIG_PATH=/opt/etls/sc_vlci/deploys
DEPLOY_SH_PATH=/opt/etls/sc_vlci/deploys/postgis/sh
DEPLOY_SQL_PATH=/opt/etls/sc_vlci/deploys/postgis/utility_scripts
DUMPS_PATH=/opt/etls/sc_vlci/postgis/dumps
LOGS_PATH=/var/log/etls/sc_vlci/postgis
TEMP_PATH=/opt/etls/sc_vlci/postgis/tmp


function setVariables() {
    # Load Variables
    source ${CONFIG_PATH}/secrets.sh

    if [ "$INSTALL_PRO" = true ]; then
        SCRIPTS_PATH=/opt/etls/sc_vlci/postgis/PRO
    fi
    if [ "$INSTALL_PRE" = true ]; then
        SCRIPTS_PATH=/opt/etls/sc_vlci/postgis/PRE
    fi
    if [ "$INSTALL_INT" = true ]; then
        SCRIPTS_PATH=/opt/etls/sc_vlci/postgis/INT
    fi

    # Script Variables
    TIMESTAMP=$(date '+%Y%m%d%-H%M')
    DAY=$(date '+%Y%m%d')

    LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
    echo "INFO - ${LOG_TIMESTAMP} - function setVariables: Variables loaded in the bash shell"
}

function setArguments() {
    # Environment to install
    INSTALL_PRO=false
    INSTALL_PRE=false
    INSTALL_INT=false

    # Database
    DUMP_DB=$2

    case $1 in
    PRO)
        INSTALL_PRO=true
        ;;
    PRE)
        INSTALL_PRE=true
        ;;
    INT)
        INSTALL_INT=true
        ;;
    *)
        echo "Uso: $ install_database (PRO|PRE|INT) (SC_VLCI|SC_VLCI_USERDB) (ALL|VLCI2|CDMGE|CDMT|GIS-SC_VLCI)" >&2
        exit 2
        ;;
    esac

    case $DUMP_DB in
    SC_VLCI)
        # Schema to install
        INSTALL_VLCI2=false
        INSTALL_CDMGE=false
        INSTALL_CDMT=false
        INSTALL_GIS_SC_VLCI=false
        case $3 in
        ALL)
            INSTALL_VLCI2=true
            INSTALL_CDMGE=true
            INSTALL_CDMT=true
            INSTALL_GIS_SC_VLCI=true
            ;;
        VLCI2)
            INSTALL_VLCI2=true
            ;;
        CDMGE)
            INSTALL_CDMGE=true
            ;;
        CDMT)
            INSTALL_CDMT=true
            ;;
        GIS-SC_VLCI)
            INSTALL_GIS_SC_VLCI=true
            ;;
        *)
            echo "Uso: $ install_database (PRO|PRE|INT) (SC_VLCI) (ALL|VLCI2|CDMGE|CDMT|GIS-SC_VLCI)" >&2
            exit 2
            ;;
        esac
        ;;
    SC_VLCI_USERDB)
        # Schema to import
        case $3 in
        ALL)
            ;;
        *)
            echo "Uso: $ install_database (PRO|PRE|INT) (SC_VLCI_USERDB) (ALL)" >&2
            exit 2
            ;;
        esac
        ;;
    *)
        echo "Uso: $ import_dump (LOCAL|PRE|INT) (SC_VLCI|SC_VLCI_USERDB) (ALL|VLCI2|CDMGE|CDMT|GIS-SC_VLCI) (DEV|REFRESH) --sqitch" >&2
        exit 2
        ;;
    esac
}

function mergeTestMain() {
    git fetch

    git checkout main
    git pull
    echo "INFO - ${LOG_TIMESTAMP} - function mergeTestMain: Main branch refreshed locally"

    git checkout test
    git pull
    echo "INFO - ${LOG_TIMESTAMP} - function mergeTestMain: Test branch refreshed locally"

    git checkout main
    git merge test
    echo "INFO - ${LOG_TIMESTAMP} - function mergeTestMain: Merged test changes to main"

    git push
    echo "INFO - ${LOG_TIMESTAMP} - function mergeTestMain: Pushed main branch"
}

function installSchema() {
    SCHEMA=$1

    if [ "$INSTALL_PRO" = true ]; then
        echo "INFO - ${LOG_TIMESTAMP} - function installSchema ${SCHEMA}: Deploying to PROD ...."

        mergeTestMain

        if [ "$SCHEMA" = "sc_vlci" ]; then
    		sqitch deploy postgis_gis_prod
        else 
            if [ "$SCHEMA" = "sc_vlci_userdb" ]; then
                sqitch deploy postgis_sc_vlci_userdb_prod
            else
                sqitch deploy postgis_prod
            fi
        fi
    fi
    if [ "$INSTALL_PRE" = true ]; then
        git checkout test
        git fetch
        git pull
        echo "INFO - ${LOG_TIMESTAMP} - function installSchema: Database scripts refreshed from github repository"

        echo "INFO - ${LOG_TIMESTAMP} - function installSchema ${SCHEMA}: Deploying to PRE ...."
        if [ "$INSTALL_GIS_SC_VLCI" = true ]; then
            sqitch deploy postgis_gis_pre
            sqitch revert postgis_gis_pre --to @HEAD^ -y
            sqitch deploy postgis_gis_pre
        else
            sqitch deploy postgis_pre
            sqitch revert postgis_pre --to @HEAD^ -y
            sqitch deploy postgis_pre
        fi
    fi

    echo "INFO - ${LOG_TIMESTAMP} - function installSchema ${SCHEMA}: Deploy completed"
}

function install() {
    if [ "$DUMP_DB" = "SC_VLCI" ]; then
        if [ "$INSTALL_VLCI2" = true ]; then
            cd ${SCRIPTS_PATH}/vlci2
            installSchema "vlci2"
            cd ${SCRIPTS_PATH}/vlci2-secrets
            installSchema "vlci2"
        fi

        if [ "$INSTALL_CDMGE" = true ]; then
            cd ${SCRIPTS_PATH}/cdmge
            installSchema "cdmge"
        fi

        if [ "$INSTALL_CDMT" = true ]; then
            cd ${SCRIPTS_PATH}/cdmt
            installSchema "cdmt"
        fi

        if [ "$INSTALL_GIS_SC_VLCI" = true ]; then
            cd ${SCRIPTS_PATH}/gis-sc_vlci
            installSchema "sc_vlci"
        fi
    else
        cd ${SCRIPTS_PATH}/sc_vlci_userdb
        installSchema "sc_vlci_userdb"
    fi
}

############################################################
# MAIN PROGRAM 
############################################################
if [ "$#" -ne 3 ]; then
    echo "Uso: $ install_database (PRO|PRE|INT) (SC_VLCI|SC_VLCI_USERDB) (ALL|VLCI2|CDMGE|CDMT|GIS-SC_VLCI)"
    exit 2
fi

setArguments $1 $2 $3

setVariables

install

exit $RESULT