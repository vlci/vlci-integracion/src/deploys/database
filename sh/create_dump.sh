#!/bin/bash
set -o pipefail
set -e
RESULT=0
trap catch_errors ERR

function catch_errors() {
    RESULT=$?
}

# Global variables
DEPLOY_SH_PATH=/opt/etls/sc_vlci/deploys/postgis/sh
CONFIG_PATH=/opt/etls/sc_vlci/deploys
SCRIPTS_PATH=/opt/etls/sc_vlci/postgis/src
DUMPS_PATH=/opt/etls/sc_vlci/postgis/dumps
LOGS_PATH=/var/log/etls/sc_vlci/postgis
TEMP_PATH=/opt/etls/sc_vlci/postgis/tmp


function setVariables() {
    # Load Variables
    source ${CONFIG_PATH}/secrets.sh

    # Script Variables
    TIMESTAMP=$(date '+%Y%m%d%-H%M')
    DAY=$(date '+%Y%m%d')

    LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
    echo "INFO - ${LOG_TIMESTAMP} - function setVariables: Variables loaded in the bash shell"
}

function setArguments() {
    # Schema to import
    DUMP_VLCI2=false
    DUMP_CDMGE=false
    DUMP_CDMT=false
    DUMP_GIS_SC_VLCI=false

    # Database
    DUMP_DB=$1
    # Export type
    TYPE=$3

    case $DUMP_DB in
    SC_VLCI)
        case $2 in
        ALL)
            DUMP_VLCI2=true
            DUMP_CDMGE=true
            DUMP_CDMT=true
            DUMP_GIS_SC_VLCI=true
            ;;
        VLCI2)
            DUMP_VLCI2=true
            ;;
        CDMGE)
            DUMP_CDMGE=true
            ;;
        CDMT)
            DUMP_CDMT=true
            ;;
        GIS-SC_VLCI)
            DUMP_GIS_SC_VLCI=true
            ;;
        *)
            echo "Uso: $ create_dump (SC_VLCI) (ALL|VLCI2|CDMGE|CDMT|GIS-SC_VLCI) (BACKUP|DEV|REFRESH)" >&2
            exit 2
            ;;
        esac

        case $TYPE in
        BACKUP)
            ;;
        DEV)
            ;;
        REFRESH)
            ;;
        *)
            echo "Uso: $ create_dump (SC_VLCI) (ALL|VLCI2|CDMGE|CDMT|GIS-SC_VLCI) (BACKUP|DEV|REFRESH)" >&2
            exit 2
            ;;
        esac
        ;;

    SC_VLCI_USERDB)
        case $2 in
        ALL)
            ;;
        *)
            echo "Uso: $ create_dump (SC_VLCI_USERDB) (ALL) (BACKUP|REFRESH)" >&2
            exit 2
            ;;
        esac

        case $TYPE in
        BACKUP)
            ;;
        REFRESH)
            ;;
        *)
            echo "Uso: $ create_dump (SC_VLCI_USERDB) (ALL) (BACKUP|REFRESH)" >&2
            exit 2
            ;;
        esac
        ;;

    *)
        echo "Uso: $ create_dump (SC_VLCI|SC_VLCI_USERDB) (ALL|VLCI2|CDMGE|CDMT|GIS-SC_VLCI) (BACKUP|DEV|REFRESH)" >&2
        exit 2
        ;;
    esac
}

function exportSchema() {
    SCHEMA=$1

    LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
    echo "INFO - ${LOG_TIMESTAMP} - function exportSchema: Creating DUMP from prod, for the schema: ${SCHEMA}"

    if [ "$DUMP_DB" = "SC_VLCI" ]; then
        if [ "$SCHEMA" = "vlci2" ]; then

            if [ "$TYPE" = "DEV" ]; then
                LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
                echo "INFO - ${LOG_TIMESTAMP} - function exportSchema: Exclude big tables"
                # EXPORT PROD SCHEMA
                PGPASSWORD=$PROD_VLCI_PASSWORD pg_dump \
                    --exclude-table-data=t_a_indicador_cubo_idioma \
                    --exclude-table-data=t_datos_cb_emt_kpi \
                    --exclude-table-data=t_f_ap \
                    --exclude-table-data=t_f_ap_avisos \
                    --exclude-table-data=t_f_ap_detail_rt \
                    --exclude-table-data=t_f_ap_urbo \
                    --exclude-table-data=t_f_conexiones_wifi_urbo \
                    --exclude-table-data=t_f_indicador_valor \
                    --exclude-table-data=t_f_poi_urbo_di \
                    --exclude-table-data=t_f_raw_from_context_broker \
                    --exclude-table-data=t_f_text_cb_weatherobserved \
                    --exclude-table-data=t_f_text_cb_wifi_kpi_conexiones \
                    --exclude-table-data=vlci_emt_tmp \
                    --host=$POSTGIS_HOST --port=$POSTGIS_PORT --username=$PROD_ADMIN_USER -c -O --if-exists --no-owner  \
                    -f $DUMPS_PATH/prod_${SCHEMA}_${TYPE}_$DAY.sql -T sc_vlci_error_log --format=p -n $SCHEMA $PROD_VLCI_DB > $LOGS_PATH/${TIMESTAMP}_${SCHEMA}_${TYPE}_dump.log 2>&1
            else
                echo "INFO - ${LOG_TIMESTAMP} - function exportSchema: Exclude cubo_idioma and old wifi tables"
                # EXPORT PROD SCHEMA
                PGPASSWORD=$PROD_VLCI_PASSWORD pg_dump \
                    --exclude-table-data=t_a_indicador_cubo_idioma \
                    --exclude-table-data=t_f_ap \
                    --exclude-table-data=t_f_ap_avisos \
                    --exclude-table-data=t_f_ap_detail_rt \
                    --exclude-table-data=t_f_ap_urbo \
                    --exclude-table-data=t_f_text_cb_wifi_kpi_conexiones \
                    --exclude-table-data=t_f_conexiones_wifi_urbo \
                    --host=$POSTGIS_HOST --port=$POSTGIS_PORT --username=$PROD_ADMIN_USER -c -O --if-exists --no-owner \
                    -f $DUMPS_PATH/prod_${SCHEMA}_${TYPE}_$DAY.sql -T sc_vlci_error_log --format=p -n $SCHEMA $PROD_VLCI_DB > $LOGS_PATH/${TIMESTAMP}_${SCHEMA}_${TYPE}_dump.log 2>&1
            fi
        else 
            if [ "$SCHEMA" = "sc_vlci" ]; then
                # EXPORT PROD GIS SC_VLCI SCHEMA
                PGPASSWORD=$PROD_GIS_SC_VLCI_PASSWORD pg_dump \
                    --exclude-table-data=trafico_any_trafficflowobserved \
                    --exclude-table-data=trafico_any_road \
                    --exclude-table-data=trafico_any_offstreetparking \
                    --host=$PROD_POSTGIS_GIS_HOST --port=$PROD_POSTGIS_GIS_PORT --username=$GIS_SC_VLCI_USER -c -O --if-exists --no-owner \
                    -f $DUMPS_PATH/prod_${SCHEMA}_${TYPE}_$DAY.sql -T sc_vlci_error_log --format=p -n $SCHEMA $GIS_SC_VLCI_DB > $LOGS_PATH/${TIMESTAMP}_${SCHEMA}_${TYPE}_dump.log 2>&1
            else
                # EXPORT PROD SCHEMA
                PGPASSWORD=$PROD_VLCI_PASSWORD pg_dump \
                    --host=$POSTGIS_HOST --port=$POSTGIS_PORT --username=$PROD_ADMIN_USER -c -O --if-exists --no-owner \
                    -f $DUMPS_PATH/prod_${SCHEMA}_${TYPE}_$DAY.sql -T sc_vlci_error_log --format=p -n $SCHEMA $PROD_VLCI_DB > $LOGS_PATH/${TIMESTAMP}_${SCHEMA}_${TYPE}_dump.log 2>&1
            fi
        fi

        LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
        echo "INFO - ${LOG_TIMESTAMP} - function exportSchema: Created DUMP from prod, for the schema: ${SCHEMA}"

        # EXPORT PROD SQITCH SCHEMA
        if [ "$SCHEMA" = "sc_vlci" ]; then
            PGPASSWORD=$PROD_GIS_SC_VLCI_PASSWORD pg_dump \
                --host=$PROD_POSTGIS_GIS_HOST --port=$PROD_POSTGIS_GIS_PORT --username=$GIS_SC_VLCI_USER -c -O --if-exists --no-owner \
                -f $DUMPS_PATH/prod_sqitch_${SCHEMA}_${TYPE}_$DAY.sql -T sc_vlci_error_log --format=p -n sqitch_$SCHEMA $GIS_SC_VLCI_DB > $LOGS_PATH/${TIMESTAMP}_sqitch_${SCHEMA}_${TYPE}_dump.log 2>&1
        else
            PGPASSWORD=$PROD_VLCI_PASSWORD pg_dump \
                --host=$POSTGIS_HOST --port=$POSTGIS_PORT --username=$PROD_ADMIN_USER -c -O --if-exists --no-owner \
                -f $DUMPS_PATH/prod_sqitch_${SCHEMA}_${TYPE}_$DAY.sql -T sc_vlci_error_log --format=p -n sqitch_$SCHEMA $PROD_VLCI_DB > $LOGS_PATH/${TIMESTAMP}_sqitch_${SCHEMA}_${TYPE}_dump.log 2>&1
        fi
        LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
        echo "INFO - ${LOG_TIMESTAMP} - function exportSchema: Created DUMP from prod, for the schema: sqitch_${SCHEMA}"

    else
        PGPASSWORD=$PROD_SC_VLCI_USERDB_PASSWORD pg_dump \
            --host=$POSTGIS_HOST --port=$POSTGIS_PORT --username=$PROD_SC_VLCI_USERDB_USER -c -O --if-exists --no-owner \
            -f $DUMPS_PATH/prod_${SCHEMA}_${TYPE}_$DAY.sql --format=p -n public $PROD_SC_VLCI_USERDB_DB > $LOGS_PATH/${TIMESTAMP}_${SCHEMA}_${TYPE}_dump.log 2>&1

        PGPASSWORD=$PROD_SC_VLCI_USERDB_PASSWORD pg_dump \
            --host=$POSTGIS_HOST --port=$POSTGIS_PORT --username=$PROD_SC_VLCI_USERDB_USER -c -O --if-exists --no-owner \
            -f $DUMPS_PATH/prod_sqitch_${SCHEMA}_${TYPE}_$DAY.sql -T sc_vlci_error_log --format=p -n sqitch_$SCHEMA $PROD_SC_VLCI_USERDB_DB > $LOGS_PATH/${TIMESTAMP}_sqitch_${SCHEMA}_${TYPE}_dump.log 2>&1

        LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
        echo "INFO - ${LOG_TIMESTAMP} - function exportSchema: Created DUMP from prod, for the database: ${PROD_SC_VLCI_USERDB_DB}"
    fi

}

function zipDump() {
    SCHEMA=$1

    # DUMPS
    gzip -f $DUMPS_PATH/prod_${SCHEMA}_${TYPE}_$DAY.sql
    gzip -f $DUMPS_PATH/prod_sqitch_${SCHEMA}_${TYPE}_$DAY.sql
}

function exportDB() {
    if [ "$DUMP_DB" = "SC_VLCI" ]; then
        if [ "$DUMP_VLCI2" = true ]; then
            exportSchema "vlci2"
            zipDump "vlci2" 
        fi

        if [ "$DUMP_CDMGE" = true ]; then
            exportSchema "cdmge"
            zipDump "cdmge" 
        fi

        if [ "$DUMP_CDMT" = true ]; then
            exportSchema "cdmt"
            zipDump "cdmt" 
        fi

        if [ "$DUMP_GIS_SC_VLCI" = true ]; then
            exportSchema "sc_vlci"
            zipDump "sc_vlci" 
        fi
    else
        exportSchema "sc_vlci_userdb"
        zipDump "sc_vlci_userdb"
    fi
}

function housekeeping() {
    # LOGS
    find $LOGS_PATH/*.log -type f -mtime +30 -delete
    find $DUMPS_PATH/*.sql.gz -type f -mtime +30 -delete

    LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
    echo "INFO - ${LOG_TIMESTAMP} - function housekeeping: Cleanup dumps and logs folders"
}

function processOutput() {

    if [ "$DUMP_DB" = "SC_VLCI" ]; then
        LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
        echo "INFO - ${LOG_TIMESTAMP} - function processOutput: Showing the output of the error file"

        if [ "$DUMP_VLCI2" = true ]; then
            echo "INFO - ${LOG_TIMESTAMP} - function processOutput: Schema VLCI2..."
            cat $LOGS_PATH/${TIMESTAMP}_vlci2_${TYPE}_dump.log | { grep FATAL || true; }
            cat $LOGS_PATH/${TIMESTAMP}_vlci2_${TYPE}_dump.log | { grep ERROR || true; }
        fi

        if [ "$DUMP_CDMGE" = true ]; then
            echo "INFO - ${LOG_TIMESTAMP} - function processOutput: Schema CDMGE..."
            cat $LOGS_PATH/${TIMESTAMP}_cdmge_${TYPE}_dump.log | { grep FATAL || true; }
            cat $LOGS_PATH/${TIMESTAMP}_cdmge_${TYPE}_dump.log | { grep ERROR || true; }
        fi

        if [ "$DUMP_CDMT" = true ]; then
            echo "INFO - ${LOG_TIMESTAMP} - function processOutput: Schema CDMT..."
            cat $LOGS_PATH/${TIMESTAMP}_cdmt_${TYPE}_dump.log | { grep FATAL || true; }
            cat $LOGS_PATH/${TIMESTAMP}_cdmt_${TYPE}_dump.log | { grep ERROR || true; }
        fi

        if [ "$DUMP_GIS_SC_VLCI" = true ]; then
            echo "INFO - ${LOG_TIMESTAMP} - function processOutput: Schema GIS SC_VLCI..."
            cat $LOGS_PATH/${TIMESTAMP}_sc_vlci_${TYPE}_dump.log | { grep FATAL || true; }
            cat $LOGS_PATH/${TIMESTAMP}_sc_vlci_${TYPE}_dump.log | { grep ERROR || true; }
        fi
    else
        echo "INFO - ${LOG_TIMESTAMP} - function processOutput: Database SC_VLCI_USERDB..."
        cat $LOGS_PATH/${TIMESTAMP}_sc_vlci_userdb_${TYPE}_dump.log | { grep FATAL || true; }
        cat $LOGS_PATH/${TIMESTAMP}_sc_vlci_userdb_${TYPE}_dump.log | { grep ERROR || true; }
    fi
}

############################################################
# MAIN PROGRAM
############################################################
if [ "$#" -ne 3 ]; then
    echo "Uso: $ create_dump (SC_VLCI|SC_VLCI_USERDB) (ALL|VLCI2|CDMGE|CDMT|GIS-SC_VLCI) (BACKUP|DEV|REFRESH)"
    exit 3
fi

setArguments $1 $2 $3

setVariables

exportDB

housekeeping

processOutput

LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
if [ $RESULT -ne 0 ]; then
    echo "ERROR - ${LOG_TIMESTAMP} - ERROR FOUND DURING THE PROCESS"
else
    echo "INFO - ${LOG_TIMESTAMP} - PROCESS COMPLETED SUCCESSFULLY"
fi

exit $RESULT
