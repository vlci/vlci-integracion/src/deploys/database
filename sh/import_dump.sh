#!/bin/bash
set -o pipefail
set -e
RESULT=0
trap catch_errors ERR

function catch_errors() {
    RESULT=$?
}

# Global variables
CONFIG_PATH=/opt/etls/sc_vlci/deploys
DEPLOY_SQL_PATH=/opt/etls/sc_vlci/deploys/database/utility_scripts
SCRIPTS_PATH=/opt/etls/sc_vlci/postgis
SCRIPTS_ENV_PATH=$SCRIPTS_PATH/PRO/vlci2/deploy/r_env_tables
SCRIPTS_SECRETS_ENV_PATH=$SCRIPTS_PATH/PRO/vlci2-secrets/deploy/r_env_tables
DUMPS_PATH=/opt/etls/sc_vlci/postgis/dumps
LOGS_PATH=/var/log/etls/sc_vlci/postgis
TEMP_PATH=/opt/etls/sc_vlci/postgis/tmp


function setVariables() {
    # Load Variables
    source ${CONFIG_PATH}/secrets.sh

    # Script Variables
    TIMESTAMP=$(date '+%Y%m%d%-H%M')
    DAY=$(date '+%Y%m%d')

    LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
    echo "INFO - ${LOG_TIMESTAMP} - function setVariables: Variables loaded in the bash shell"
}

function setArguments() {
    # Environment to import
    IMPORT_PRE=false
    IMPORT_INT=false
    IMPORT_LOCAL=false

    # Database
    DUMP_DB=$2

    # Import type
    TYPE=$4

    SQITCH=false
    if [ "$#" -eq 5 ]; then
        if [ "$5" != "--sqitch" ]; then
            echo "Uso: $ import_dump (LOCAL|PRE|INT) (SC_VLCI) (ALL|VLCI2|CDMGE|CDMT|GIS-SC_VLCI) (DEV|REFRESH) --sqitch" >&2
            exit 2
        else
            SQITCH=true
        fi
    fi    

    case $1 in
    PRE)
        IMPORT_PRE=true
        ;;
    INT)
        IMPORT_INT=true
        ;;
    LOCAL)
        IMPORT_LOCAL=true
        ;;
    *)
        echo "Uso: $ import_dump (LOCAL|PRE|INT) (SC_VLCI) (ALL|VLCI2|CDMGE|CDMT|GIS-SC_VLCI) (DEV|REFRESH) --sqitch" >&2
        exit 2
        ;;
    esac

    case $TYPE in
    DEV)
        ;;
    REFRESH)
        ;;
    *)
        echo "Uso: $ import_dump (LOCAL|PRE|INT) (SC_VLCI) (ALL|VLCI2|CDMGE|CDMT|GIS-SC_VLCI) (DEV|REFRESH) --sqitch" >&2
        exit 2
        ;;
    esac


    case $DUMP_DB in
    SC_VLCI)
        # Schema to import
        IMPORT_VLCI2=false
        IMPORT_CDMGE=false
        IMPORT_CDMT=false
        IMPORT_GIS_SC_VLCI=false
        case $3 in
        ALL)
            IMPORT_VLCI2=true
            IMPORT_CDMGE=true
            IMPORT_CDMT=true
            IMPORT_GIS_SC_VLCI=true
            ;;
        VLCI2)
            IMPORT_VLCI2=true
            ;;
        CDMGE)
            IMPORT_CDMGE=true
            ;;
        CDMT)
            IMPORT_CDMT=true
            ;;
        GIS-SC_VLCI)
            IMPORT_GIS_SC_VLCI=true
            ;;
        *)
            echo "Uso: $ import_dump (LOCAL|PRE|INT) (SC_VLCI) (ALL|VLCI2|CDMGE|CDMT|GIS-SC_VLCI) (DEV|REFRESH) --sqitch" >&2
            exit 2
            ;;
        esac
        ;;
    SC_VLCI_USERDB)
        # Schema to import
        case $3 in
        ALL)
            ;;
        *)
            echo "Uso: $ import_dump (LOCAL) (SC_VLCI_USERDB) (ALL) (REFRESH) --sqitch" >&2
            exit 2
            ;;
        esac
        ;;
    *)
        echo "Uso: $ import_dump (LOCAL|PRE|INT) (SC_VLCI|SC_VLCI_USERDB) (ALL|VLCI2|CDMGE|CDMT|GIS-SC_VLCI) (DEV|REFRESH) --sqitch" >&2
        exit 2
        ;;
    esac
}

function updateDBScripts() {
    for dir in ${SCRIPTS_PATH}/PRO/*/     # list directories 
    do
        echo "INFO - function updateDBScripts: pull from ${dir}.git"
        cd ${dir}
        git pull
    done

    LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
    echo "INFO - ${LOG_TIMESTAMP} - function updateDBScripts: Database scripts refreshed from github repository"
}

function prepare_export_for_env() {
    SCHEMA=$1

    LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
    echo "INFO - ${LOG_TIMESTAMP} - function prepare_export_for_env: Creating dump sql file for env: ${ENV} schema: ${SCHEMA} type: ${TYPE}..."

    # PREPARE IMPORT
    cp $DUMPS_PATH/prod_${SCHEMA}_${TYPE}_$DAY.sql $DUMPS_PATH/${ENV}_${SCHEMA}_${TYPE}_dump.sql

    # FIX DUMP FOR ALL SCHEMAS
    sed -i -e "/DROP SCHEMA IF EXISTS/d" -e "/ALTER DEFAULT PRIVILEGES FOR ROLE/d" $DUMPS_PATH/${ENV}_${SCHEMA}_${TYPE}_dump.sql >> $LOGS_PATH/${TIMESTAMP}_${ENV}_${SCHEMA}_${TYPE}_conversion.log 2>&1

    if [ "$DUMP_DB" = "SC_VLCI" ]; then
        # FIX DUMP FOR VLCI SCHEMAS (Exclude GIS)
        if [ "$SCHEMA" != "sc_vlci" ] && [ "$SCHEMA" != "sqitch_sc_vlci" ]; then
            sed -i -e "/$DIGITALTWIN_USER/d" $DUMPS_PATH/${ENV}_${SCHEMA}_${TYPE}_dump.sql >> $LOGS_PATH/${TIMESTAMP}_${ENV}_${SCHEMA}_${TYPE}_conversion.log 2>&1
            sed -i -e "s/CREATE SCHEMA/CREATE SCHEMA IF NOT EXISTS/g" $DUMPS_PATH/${ENV}_${SCHEMA}_${TYPE}_dump.sql >> $LOGS_PATH/${TIMESTAMP}_${ENV}_${SCHEMA}_${TYPE}_conversion.log 2>&1
            sed -i -e "/GRANT ALL ON SCHEMA ${SCHEMA} TO mb3_dev/d" -e "/GRANT ALL ON SCHEMA ${SCHEMA} TO vlci_admin/d" $DUMPS_PATH/${ENV}_${SCHEMA}_${TYPE}_dump.sql > $LOGS_PATH/${TIMESTAMP}_${ENV}_${SCHEMA}_${TYPE}_conversion.log 2>&1
            sed -i -e "/GRANT ALL ON SCHEMA ${SCHEMA} TO vlci_pentaho/d" -e "/GRANT ALL ON SCHEMA ${SCHEMA} TO role_sc_vlci/d" -e "/GRANT USAGE ON SCHEMA ${SCHEMA} TO role_sc_vlci/d" $DUMPS_PATH/${ENV}_${SCHEMA}_${TYPE}_dump.sql >> $LOGS_PATH/${TIMESTAMP}_${ENV}_${SCHEMA}_${TYPE}_conversion.log 2>&1
            sed -i -e "/ALTER DEFAULT PRIVILEGES FOR ROLE/d" $DUMPS_PATH/${ENV}_${SCHEMA}_${TYPE}_dump.sql >> $LOGS_PATH/${TIMESTAMP}_${ENV}_${SCHEMA}_${TYPE}_conversion.log 2>&1
            sed -i -e "s/CREATE PROCEDURE/CREATE OR REPLACE PROCEDURE/g" $DUMPS_PATH/${ENV}_${SCHEMA}_${TYPE}_dump.sql >> $LOGS_PATH/${TIMESTAMP}_${ENV}_${SCHEMA}_${TYPE}_conversion.log 2>&1
            sed -i -e "s/$PROD_VLCI_DB/$VLCI_DB/g" -e "s/$PROD_MB3_USER/$MB3_USER/g" -e "s/$PROD_ADMIN_USER/$ADMIN_USER/g" -e "s/$PROD_PTHO_USER/$PTHO_USER/g" $DUMPS_PATH/${ENV}_${SCHEMA}_${TYPE}_dump.sql >> $LOGS_PATH/${TIMESTAMP}_${ENV}_${SCHEMA}_${TYPE}_conversion.log 2>&1
            sed -i -e "/$PROD_VLCI_USER/d" -e "/$IBM_USER/d" -e "/$WARPCOM_USER/d" -e "/$DIGITALTWIN_USER/d" $DUMPS_PATH/${ENV}_${SCHEMA}_${TYPE}_dump.sql >> $LOGS_PATH/${TIMESTAMP}_${ENV}_${SCHEMA}_${TYPE}_conversion.log 2>&1
        else
            sed -i -e "/CREATE SCHEMA/d" $DUMPS_PATH/${ENV}_${SCHEMA}_${TYPE}_dump.sql >> $LOGS_PATH/${TIMESTAMP}_${ENV}_${SCHEMA}_${TYPE}_conversion.log 2>&1
            sed -i -e "/GRANT ALL ON SCHEMA ${SCHEMA} TO gis/d" -e "/GRANT ALL ON SCHEMA ${SCHEMA} TO plataformavlci/d" $DUMPS_PATH/${ENV}_${SCHEMA}_${TYPE}_dump.sql >> $LOGS_PATH/${TIMESTAMP}_${ENV}_${SCHEMA}_${TYPE}_conversion.log 2>&1
        fi
    else
        sed -i -e "/CREATE SCHEMA/d" $DUMPS_PATH/${ENV}_${SCHEMA}_${TYPE}_dump.sql >> $LOGS_PATH/${TIMESTAMP}_${ENV}_${SCHEMA}_${TYPE}_conversion.log 2>&1
    fi

    LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
    echo "INFO - ${LOG_TIMESTAMP} - function prepare_export_for_env: Dump created"
}

function prepare_pre() {
    ENV=pre
    ENVUPPER=PRE
    VLCI_DB=$PRE_VLCI_DB
    MB3_USER=$PRE_MB3_USER
    ADMIN_USER=$PRE_ADMIN_USER
    PTHO_USER=$PRE_PTHO_USER
    VLCI_PASSWORD=$PRE_VLCI_PASSWORD
    GIS_SC_VLCI_PASSWORD=$PRE_GIS_SC_VLCI_PASSWORD
    POSTGIS_GIS_HOST=$PRE_POSTGIS_GIS_HOST
    POSTGIS_GIS_PORT=$PRE_POSTGIS_GIS_PORT
}

function prepare_int() {
    ENV=int
    ENVUPPER=INT
    VLCI_DB=$INT_VLCI_DB
    MB3_USER=$INT_MB3_USER
    ADMIN_USER=$INT_ADMIN_USER
    PTHO_USER=$INT_PTHO_USER
    VLCI_PASSWORD=$INT_VLCI_PASSWORD
}

function prepare_local() {
    ENV=local
    ENVUPPER=LOCAL
    VLCI_DB=$LOCAL_VLCI_DB
    MB3_USER=$LOCAL_MB3_USER
    ADMIN_USER=$LOCAL_ADMIN_USER
    PTHO_USER=$LOCAL_PTHO_USER
    VLCI_PASSWORD=$LOCAL_VLCI_PASSWORD
    GIS_SC_VLCI_PASSWORD=$LOCAL_GIS_SC_VLCI_PASSWORD
    GIS_SC_VLCI_USER=$LOCAL_GIS_SC_VLCI_USER
    POSTGIS_GIS_HOST=$LOCAL_POSTGIS_GIS_HOST
    POSTGIS_GIS_PORT=$LOCAL_POSTGIS_GIS_PORT
}

function clean_env() {
    SCHEMA=$1

    LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
    echo "INFO - ${LOG_TIMESTAMP} - function clean_env: Droping all objects for env: ${ENV} schema: ${SCHEMA} type: ${TYPE}..."

    # BEFORE IMPORT
    cp $DEPLOY_SQL_PATH/drop.all.execution.sql $TEMP_PATH/exec.sql
    sed -i -e "s/<schema>/${SCHEMA}/g" $TEMP_PATH/exec.sql

    if [ "$DUMP_DB" = "SC_VLCI" ]; then
        if [ "$SCHEMA" = "sc_vlci" ] || [ "$SCHEMA" = "sqitch_sc_vlci" ]; then
            PGPASSWORD=$GIS_SC_VLCI_PASSWORD psql -h $POSTGIS_GIS_HOST -p $POSTGIS_GIS_PORT -U $GIS_SC_VLCI_USER \
                -f $DEPLOY_SQL_PATH/drop.all.function_gis.sql \
                -f $TEMP_PATH/exec.sql \
                $GIS_SC_VLCI_DB \
                > $LOGS_PATH/${TIMESTAMP}_${ENV}_${SCHEMA}_${TYPE}_before.log 2>&1
        else
            PGPASSWORD=$VLCI_PASSWORD psql -h $POSTGIS_HOST -p $POSTGIS_PORT -U $ADMIN_USER \
                -f $DEPLOY_SQL_PATH/drop.all.function.sql \
                -f $TEMP_PATH/exec.sql \
                $VLCI_DB \
                > $LOGS_PATH/${TIMESTAMP}_${ENV}_${SCHEMA}_${TYPE}_before.log 2>&1
        fi
    else
        PGPASSWORD=$PROD_SC_VLCI_USERDB_PASSWORD psql -h $POSTGIS_HOST -p $POSTGIS_PORT -U $PROD_SC_VLCI_USERDB_USER \
            -f $DEPLOY_SQL_PATH/drop.all.function.sql \
            -f $TEMP_PATH/exec.sql \
            $VLCI_DB \
            > $LOGS_PATH/${TIMESTAMP}_${ENV}_${SCHEMA}_${TYPE}_before.log 2>&1
    fi
    rm $TEMP_PATH/exec.sql

    LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
    echo "INFO - ${LOG_TIMESTAMP} - function clean_env: Dropped all objects"
}

function import_env() {
    SCHEMA=$1

    LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
    echo "INFO - ${LOG_TIMESTAMP} - function import_env: Importing dump into the env: ${ENV} schema: ${SCHEMA} type: ${TYPE} ..."

    # IMPORT
    if [ "$DUMP_DB" = "SC_VLCI" ]; then
        if [ "$SCHEMA" = "sc_vlci" ] || [ "$SCHEMA" = "sqitch_sc_vlci" ]; then
            PGPASSWORD=$GIS_SC_VLCI_PASSWORD psql -h $POSTGIS_GIS_HOST -p $POSTGIS_GIS_PORT -U $GIS_SC_VLCI_USER \
                -f $DUMPS_PATH/${ENV}_${SCHEMA}_${TYPE}_dump.sql \
                $GIS_SC_VLCI_DB \
                > $LOGS_PATH/${TIMESTAMP}_${ENV}_${SCHEMA}_${TYPE}_import.log 2>&1
        else
            PGPASSWORD=$VLCI_PASSWORD psql -h $POSTGIS_HOST -p $POSTGIS_PORT -U $ADMIN_USER \
                -f $DUMPS_PATH/${ENV}_${SCHEMA}_${TYPE}_dump.sql \
                $VLCI_DB \
                > $LOGS_PATH/${TIMESTAMP}_${ENV}_${SCHEMA}_${TYPE}_import.log 2>&1
        fi
    else
        PGPASSWORD=$PROD_SC_VLCI_USERDB_PASSWORD psql -h $POSTGIS_HOST -p $POSTGIS_PORT -U $PROD_SC_VLCI_USERDB_USER \
            -f $DUMPS_PATH/${ENV}_${SCHEMA}_${TYPE}_dump.sql \
            $PROD_SC_VLCI_USERDB_DB \
            > $LOGS_PATH/${TIMESTAMP}_${ENV}_${SCHEMA}_${TYPE}_import.log 2>&1
    fi

    LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
    echo "INFO - ${LOG_TIMESTAMP} - function import_env: Dump imported"
}

function post_import_env_vlci2() {
    SCHEMA=$1

    LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
    echo "INFO - ${LOG_TIMESTAMP} - function post_import_env_vlci2: Environment specific scripts applied in env: ${ENV} schema: ${SCHEMA} type: ${TYPE}..."

    PGPASSWORD=$VLCI_PASSWORD psql -h $POSTGIS_HOST -p $POSTGIS_PORT -U $ADMIN_USER \
        -f $SCRIPTS_ENV_PATH/tb.t_d_api_loader.sql \
        -f $SCRIPTS_SECRETS_ENV_PATH/tb.t_d_conexiones_bbdd.sql \
        -f $SCRIPTS_SECRETS_ENV_PATH/tb.t_d_conexiones_ftp.sql \
        -f $SCRIPTS_ENV_PATH/tb.t_d_parametros_etl_carga.sql \
        -f $SCRIPTS_ENV_PATH/tb.t_d_parametros.sql \
        $VLCI_DB > $LOGS_PATH/${TIMESTAMP}_${ENV}_${SCHEMA}_${TYPE}_environment_load.log 2>&1

    LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
    echo "INFO - ${LOG_TIMESTAMP} - function post_import_env_vlci2: Environment specific scripts applied"
}

function recoverDump() {
    SCHEMA=$1

    if [ -f "$DUMPS_PATH/prod_${SCHEMA}_${TYPE}_$DAY.sql.gz" ]; then
        gzip -d -f $DUMPS_PATH/prod_${SCHEMA}_${TYPE}_$DAY.sql.gz
    else
        echo "ERROR. Cannot find a valid dump file. File expected: $DUMPS_PATH/prod_${SCHEMA}_${TYPE}_$DAY.sql.gz"
        exit 3
    fi
    
}

function importSchema() {
    SCHEMA=$1

    recoverDump $SCHEMA

    prepare_export_for_env $SCHEMA
    clean_env $SCHEMA
    import_env $SCHEMA
    if [ "$SCHEMA" = "vlci2" ]; then
        post_import_env_vlci2 $SCHEMA
    fi

    gzip -f $DUMPS_PATH/prod_${SCHEMA}_${TYPE}_$DAY.sql
}

function refreshMatViews() {
    git -C /opt/etls/sc_vlci/${ENVUPPER}/shscripts/postgis-sh/ pull 
    /opt/etls/sc_vlci/${ENVUPPER}/shscripts/postgis-sh/pg_refresh_views.sh ${ENVUPPER}

    LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
    echo "INFO - ${LOG_TIMESTAMP} - function refreshMatViews: Materialized Views refreshed in ${ENVUPPER} environment"
}

function import() {
    if [ "$IMPORT_PRE" = true ]; then
        prepare_pre
    fi

    if [ "$IMPORT_INT" = true ]; then
        prepare_int 
    fi

    if [ "$IMPORT_LOCAL" = true ]; then
        prepare_local
        if [ "$IMPORT_GIS_SC_VLCI" = false ]; then
            create_schemas_and_roles
        fi
    fi


    if [ "$DUMP_DB" = "SC_VLCI" ]; then
        if [ "$IMPORT_VLCI2" = true ]; then
            importSchema "vlci2"
            refreshMatViews
            if [ "$SQITCH" = true ]; then
                importSchema "sqitch_vlci2"
            fi
        fi

        if [ "$IMPORT_CDMGE" = true ]; then
            importSchema "cdmge"
            if [ "$SQITCH" = true ]; then
                importSchema "sqitch_cdmge"
            fi
        fi

        if [ "$IMPORT_CDMT" = true ]; then
            importSchema "cdmt"
            if [ "$SQITCH" = true ]; then
                importSchema "sqitch_cdmt"
            fi
        fi

        if [ "$IMPORT_GIS_SC_VLCI" = true ]; then
            importSchema "sc_vlci"
            if [ "$SQITCH" = true ]; then
                importSchema "sqitch_sc_vlci"
            fi
        fi
    else
        if [ "$IMPORT_LOCAL" = true ]; then
            importSchema "sc_vlci_userdb"
            if [ "$SQITCH" = true ]; then
                importSchema "sqitch_sc_vlci_userdb"
            fi
        fi
    fi
}

function create_schemas_and_roles() {
    LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
    echo "INFO - ${LOG_TIMESTAMP} - function create_schemas_and_roles: Init"

    # EXECUTE
    PGPASSWORD=$VLCI_PASSWORD psql -h $POSTGIS_HOST -p $POSTGIS_PORT -U $ADMIN_USER \
        -f $DEPLOY_SQL_PATH/local.sql \
        $VLCI_DB \
        > $LOGS_PATH/${TIMESTAMP}_${ENV}_create_schemas_roles.log 2>&1

    LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
    echo "INFO - ${LOG_TIMESTAMP} - function create_schemas_and_roles: End"
}

function housekeeping() {
    # DUMPS
    if compgen -G "${DUMPS_PATH}/*.sql" > /dev/null; then
        rm $DUMPS_PATH/*.sql
    fi

    # LOGS
    find $LOGS_PATH/* -type f -mtime +30 -delete
    find $DUMPS_PATH/*.sql.gz -type f -mtime +30 -delete

    LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
    echo "INFO - ${LOG_TIMESTAMP} - function housekeeping: Cleanup dumps and logs folders"
}

function processOutput() {
    if compgen -G "${LOGS_PATH}/${TIMESTAMP}*.log" > /dev/null; then
        cat $LOGS_PATH/${TIMESTAMP}*.log | { grep FATAL || true; } > $LOGS_PATH/${TIMESTAMP}_${TYPE}_import.err
        cat $LOGS_PATH/${TIMESTAMP}*.log | { grep ERROR || true; } >> $LOGS_PATH/${TIMESTAMP}_${TYPE}_import.err
        cat $LOGS_PATH/${TIMESTAMP}*.log | { grep WARNING || true; } > $LOGS_PATH/${TIMESTAMP}_${TYPE}_import.warn
        LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
        echo "INFO - ${LOG_TIMESTAMP} - function processOutput: Review file $LOGS_PATH/${TIMESTAMP}_${TYPE}_import.err (and same with .warn for warnings)"
        echo "INFO - ${LOG_TIMESTAMP} - function processOutput: Showing the output of the error file..."

        cat $LOGS_PATH/${TIMESTAMP}_${TYPE}_import.err
    fi
}

############################################################
# MAIN PROGRAM
############################################################
if [ "$#" -ne 4 ]; then
    if [ "$#" -ne 5 ]; then
        echo "Uso: $ import_dump (LOCAL|PRE|INT) (SC_VLCI|SC_VLCI_USERDB) (ALL|VLCI2|CDMGE|CDMT|GIS-SC_VLCI) (DEV|REFRESH) --sqitch"
        exit 2
    else
        setArguments $1 $2 $3 $4 $5
    fi
else
    setArguments $1 $2 $3 $4
fi

setVariables

updateDBScripts

import

housekeeping

processOutput

LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
if [ -s $LOGS_PATH/${TIMESTAMP}_${TYPE}_import.err ]; then
    # Error file with data, exit with error
    echo "ERROR - ${LOG_TIMESTAMP} - ERROR FOUND DURING THE PROCESS"
    RESULT=1
else
    # Error file without data
    echo "INFO - ${LOG_TIMESTAMP} - PROCESS COMPLETED SUCCESSFULLY"
fi

exit $RESULT