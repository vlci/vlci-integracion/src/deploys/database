DO
$do1$
BEGIN
   IF NOT EXISTS (
      SELECT FROM pg_catalog.pg_roles  -- SELECT list can be empty for this
      WHERE  rolname = 'cygnus_vlci') THEN

      CREATE ROLE cygnus_vlci;
   END IF;
END
$do1$;


DO
$do2$
BEGIN
   IF NOT EXISTS (
      SELECT FROM pg_catalog.pg_roles  -- SELECT list can be empty for this
      WHERE  rolname = 'role_sc_vlci_readwrite') THEN

      CREATE ROLE role_sc_vlci_readwrite;
   END IF;
END
$do2$;

DO
$do3$
BEGIN
   IF NOT EXISTS (
      SELECT FROM pg_catalog.pg_roles  -- SELECT list can be empty for this
      WHERE  rolname = 'role_sc_vlci_readonly') THEN

      CREATE ROLE role_sc_vlci_readonly;
   END IF;
END
$do3$;

CREATE SCHEMA IF NOT EXISTS vlci2;
CREATE SCHEMA IF NOT EXISTS cdmge;
CREATE SCHEMA IF NOT EXISTS cdmt;
CREATE SCHEMA IF NOT EXISTS sqitch_vlci2;
CREATE SCHEMA IF NOT EXISTS sqitch_cdmge;
CREATE SCHEMA IF NOT EXISTS sqitch_cdmt;