#!/bin/bash
set -o pipefail
set -e
RESULT=0
trap catch_errors ERR

function catch_errors() {
    RESULT=$?
}

# Global variables
CONFIG_PATH=/opt/etls/sc_vlci/deploys
DEPLOY_SH_PATH=/opt/etls/sc_vlci/deploys/postgis/sh
DEPLOY_SQL_PATH=/opt/etls/sc_vlci/deploys/postgis/utility_scripts
DUMPS_PATH=/opt/etls/sc_vlci/postgis/dumps
LOGS_PATH=/var/log/etls/sc_vlci/postgis
TEMP_PATH=/opt/etls/sc_vlci/postgis/tmp


function setVariables() {
    # Load Variables
    source ${CONFIG_PATH}/secrets.sh

    SCRIPTS_PATH=/opt/etls/sc_vlci/postgis/PRO

    # Script Variables
    TIMESTAMP=$(date '+%Y%m%d%-H%M')
    DAY=$(date '+%Y%m%d')

    LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
    echo "INFO - ${LOG_TIMESTAMP} - function setVariables: Variables loaded in the bash shell"
}

function setArguments() {
    # Database
    DUMP_DB=$1

    case $DUMP_DB in
    SC_VLCI)
        # Schema to install
        INSTALL_VLCI2=false
        INSTALL_CDMGE=false
        INSTALL_CDMT=false
        INSTALL_GIS_SC_VLCI=false
        case $2 in
        ALL)
            INSTALL_VLCI2=true
            INSTALL_CDMGE=true
            INSTALL_CDMT=true
            INSTALL_GIS_SC_VLCI=true
            ;;
        VLCI2)
            INSTALL_VLCI2=true
            ;;
        CDMGE)
            INSTALL_CDMGE=true
            ;;
        CDMT)
            INSTALL_CDMT=true
            ;;
        GIS-SC_VLCI)
            INSTALL_GIS_SC_VLCI=true
            ;;
        *)
            echo "Uso: $ tag_database (SC_VLCI) (ALL|VLCI2|CDMGE|CDMT|GIS-SC_VLCI) {TAG_NAME} {COMMENT}" >&2
            exit 2
            ;;
        esac
        ;;
    SC_VLCI_USERDB)
        # Schema to import
        case $2 in
        ALL)
            ;;
        *)
            echo "Uso: $ tag_database (SC_VLCI_USERDB) (ALL) {TAG_NAME} {COMMENT}" >&2
            exit 2
            ;;
        esac
        ;;
    *)
        echo "Uso: $ tag_database (SC_VLCI|SC_VLCI_USERDB) (ALL|VLCI2|CDMGE|CDMT|GIS-SC_VLCI) {TAG_NAME} {COMMENT}" >&2
        exit 2
        ;;
    esac
}

function tagSchema() {
    TAG_NAME=$1
    COMMENT=$2

    git fetch
    echo "INFO - ${LOG_TIMESTAMP} - function tagSchema: Database scripts refreshed from github repository"

    git checkout test
    git pull
    echo "INFO - ${LOG_TIMESTAMP} - function tagSchema: Test branch refreshed locally"

    sqitch tag ${TAG_NAME} -m "${COMMENT}"
    git add sqitch.plan
    git commit -m "${COMMENT}"
    echo "INFO - ${LOG_TIMESTAMP} - function tagSchema: Committed new sqitch tag"

    git push
    echo "INFO - ${LOG_TIMESTAMP} - function tagSchema: Pushed test branch"

    git checkout main
    git merge test -m "${COMMENT}" --log --no-edit
    echo "INFO - ${LOG_TIMESTAMP} - function tagSchema: Merged test changes to main"

    git push
    echo "INFO - ${LOG_TIMESTAMP} - function tagSchema: Pushed main branch"

    git tag -a ${TAG_NAME} -m "${COMMENT}"
    git push origin ${TAG_NAME}
    echo "INFO - ${LOG_TIMESTAMP} - function tagSchema: Pushed tag in main branch"
}

function tag() {
    TAG_NAME=$1
    COMMENT=$2

    if [ "$DUMP_DB" = "SC_VLCI" ]; then
        if [ "$INSTALL_VLCI2" = true ]; then
            cd ${SCRIPTS_PATH}/vlci2
            tagSchema $TAG_NAME $COMMENT
            cd ${SCRIPTS_PATH}/vlci2-secrets
            tagSchema $TAG_NAME $COMMENT
        fi

        if [ "$INSTALL_CDMGE" = true ]; then
            cd ${SCRIPTS_PATH}/cdmge
            tagSchema $TAG_NAME $COMMENT
        fi

        if [ "$INSTALL_CDMT" = true ]; then
            cd ${SCRIPTS_PATH}/cdmt
            tagSchema $TAG_NAME $COMMENT
        fi

        if [ "$INSTALL_GIS_SC_VLCI" = true ]; then
            cd ${SCRIPTS_PATH}/gis-sc_vlci
            tagSchema $TAG_NAME $COMMENT
        fi
    else
        cd ${SCRIPTS_PATH}/sc_vlci_userdb
        tagSchema $TAG_NAME $COMMENT
    fi

}

############################################################
# MAIN PROGRAM 
############################################################
if [ "$#" -ne 4 ]; then
    echo "Uso: $ tag_database (SC_VLCI|SC_VLCI_USERDB) (ALL|VLCI2|CDMGE|CDMT|GIS-SC_VLCI) {TAG_NAME} {COMMENT}"
    exit 2
fi

setArguments $1 $2

setVariables

tag $3 $4

exit $RESULT