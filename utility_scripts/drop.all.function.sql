---
CREATE OR REPLACE FUNCTION drop_DB_objects(schema_name text)
RETURNS VOID AS $$

DECLARE 
 rd_object RECORD;
 v_idx_statement VARCHAR(500);  

BEGIN

--1. Dropping all materialized views
 RAISE NOTICE '%', 'Dropping all materialized views...'; 
 FOR rd_object IN ( SELECT matviewname 
        FROM pg_matviews 
       WHERE schemaname = schema_name
    )
 LOOP      
  v_idx_statement = 'DROP MATERIALIZED VIEW IF EXISTS ' || schema_name || '.' || rd_object.matviewname || ' CASCADE';
  RAISE NOTICE '%', v_idx_statement;
  EXECUTE v_idx_statement;
 END LOOP; 
 RAISE NOTICE '%', 'Done. Droped all views...';

--2. Dropping all views
 RAISE NOTICE '%', 'Dropping all views...'; 
 FOR rd_object IN ( SELECT viewname 
        FROM pg_views 
       WHERE schemaname = schema_name
    )
 LOOP      
  v_idx_statement = 'DROP VIEW IF EXISTS ' || schema_name || '.' || rd_object.viewname || ' CASCADE';
  RAISE NOTICE '%', v_idx_statement;
  EXECUTE v_idx_statement;
 END LOOP; 
 RAISE NOTICE '%', 'Done. Droped all views...';

 --3. Dropping all table objects
 RAISE NOTICE '%', 'Dropping all table objects...';
 -- Drop child partitions first and then the base tables.
 FOR rd_object IN (
    with child as (
      SELECT c.relname AS tablename, 0 as parent
      FROM   pg_inherits 
      JOIN   pg_class AS c ON (inhrelid=c.oid)
      WHERE  c.relnamespace IN ( SELECT oid FROM   pg_namespace WHERE nspname = schema_name)
         and c.relname not like 'sc_vlci%_error_log'
    )
    select * from child
    union all        
    SELECT tablename, 1 as parent FROM pg_tables 
     WHERE schemaname = schema_name
       AND tablename not in (select inn.tablename from child inn)
       and tablename not like 'sc_vlci%_error_log'
     order by parent  
 )
 LOOP

  v_idx_statement = 'DROP TABLE IF EXISTS ' || schema_name || '.' || rd_object.tablename || ' CASCADE';
  RAISE NOTICE '%', v_idx_statement;
  EXECUTE v_idx_statement;
 END LOOP; 
 RAISE NOTICE '%', 'Done. Droped all table objects...';

 --4. Dropping all Sequence objects
 RAISE NOTICE '%', 'Dropping all Sequence objects...'; 
 FOR rd_object IN ( SELECT sequence_name 
        FROM information_schema.sequences 
       WHERE sequence_schema = schema_name
    )
 LOOP      

  v_idx_statement = 'DROP SEQUENCE IF EXISTS ' || schema_name || '.' || rd_object.sequence_name || ' CASCADE';
  RAISE NOTICE '%', v_idx_statement;
  EXECUTE v_idx_statement;
 END LOOP; 
 RAISE NOTICE '%', 'Done. Droped all sequences...';

  ---5. Dropping all functions
 RAISE NOTICE '%', 'Dropping all stored functions...'; 
 FOR rd_object IN ( select format('%I.%I(%s)', n.nspname, p.proname, oidvectortypes(p.proargtypes)) as functionDef 
        from pg_proc p
        left join pg_namespace n on p.pronamespace = n.oid
        left join pg_language l on p.prolang = l.oid
        left join pg_type t on t.oid = p.prorettype 
        where n.nspname not in ('pg_catalog', 'information_schema')
            and p.prokind = 'f'
            and n.nspname = schema_name
    )
 LOOP      

  v_idx_statement = 'DROP FUNCTION IF EXISTS ' || rd_object.functionDef || ' CASCADE';
  RAISE NOTICE '%', v_idx_statement;
  EXECUTE v_idx_statement;
  
 END LOOP; 
 RAISE NOTICE '%', 'Done. Droped all stored functions...';
 
 ---6. Dropping all procedures
 RAISE NOTICE '%', 'Dropping all stored procedures...'; 
 FOR rd_object IN ( select format('%I.%I(%s)', n.nspname, p.proname, oidvectortypes(p.proargtypes)) as functionDef 
        from pg_proc p
        left join pg_namespace n on p.pronamespace = n.oid
        left join pg_language l on p.prolang = l.oid
        left join pg_type t on t.oid = p.prorettype 
        where n.nspname not in ('pg_catalog', 'information_schema')
            and p.prokind = 'p'
            and n.nspname = schema_name
    )
 LOOP      

  v_idx_statement = 'DROP PROCEDURE IF EXISTS ' || rd_object.functionDef || ' CASCADE';
  RAISE NOTICE '%', v_idx_statement;
  EXECUTE v_idx_statement;
  
 END LOOP; 
 RAISE NOTICE '%', 'Done. Droped all stored procedures...';
 
 
END;
$$  LANGUAGE plpgsql;